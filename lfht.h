
#ifndef __LFHT_H__
#define __LFHT_H__

#include <stddef.h>
#include <threads.h>

#ifndef LFHT_DEBUG
#define LFHT_DEBUG 0
#endif

#if LFHT_DEBUG

#define MAX_NODES 2
#define HASH_SIZE 2
#define ROOT_HASH_SIZE HASH_SIZE
//2 works for tests if the hash values never exceed 32 bits
#define MR_FREQUENCY 1
#define MR_THRESHOLD 1
#else

#define MAX_NODES 3
//ROOT_HASH_SIZE + ((WORD_SIZE/4)-1) * HASH_SIZE needs to be >= WORD_SIZE
#define ROOT_HASH_SIZE 16
#define HASH_SIZE 4

#define MR_FREQUENCY 256
#define MR_THRESHOLD 256

#endif

#define WORD_SIZE 64
#define CACHE_LINE_SIZE 64


struct lfht_head {
	struct lfht_node *entry_hash;
	int root_hnode_size;
	int hnode_size;
	int max_chain_nodes;
	int mr_frequency;
	int mr_threshold;
	int max_threads;
	struct mr_entry *thread_array;
	tss_t tss_key;
};


struct lfht_head *init_lfht(
		int max_threads);

struct lfht_head *init_lfht_explicit(
		int root_hnode_size,
		int hnode_size,
		int max_chain_nodes,
		int mr_frequency,
		int mr_threshold,
		int max_threads);

//this function is multithread-unsafe!
void free_lfht_mtu(
		struct lfht_head *head);

void *lfht_search(
		struct lfht_head *head,
		size_t hash);

int lfht_search_explicit(
		struct lfht_head *head,
		size_t hash,
		void **result);

void *lfht_insert(
		struct lfht_head *head,
		size_t hash,
		void *value);

void *lfht_remove(
		struct lfht_head *head,
		size_t hash);

int lfht_remove_explicit(
		struct lfht_head *head,
		size_t hash,
		void **result);

//debug interface


void *lfht_debug_search(
		struct lfht_head *head,
		size_t hash);

#endif // __LFHT_H__

