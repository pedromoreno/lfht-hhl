CC=gcc
CFLAGS=-Wall -fPIC
AR=ar
OPT= -O3
INCLUDE?=-I.
LFLAGS=-shared
DEBUG= -g -ggdb -Og -DLFHT_DEBUG=1

.PHONY: default debug all clean

default: liblfht.a
debug: liblfht_debug.a liblfht_debug.so
all: liblfht.a liblfht.so

liblfht.so: lfht.o
	$(CC) lfht.o $(CFLAGS) $(INCLUDE) $(OPT) $(LFLAGS) -o liblfht.so

liblfht.a: lfht.o
	$(AR) rcu liblfht.a lfht.o

lfht.o: lfht.c
	$(CC) -c lfht.c $(CFLAGS) $(INCLUDE) $(OPT) $(LFLAGS)

liblfht_debug.so: lfht_debug.o
	$(CC) lfht_debug.o $(CFLAGS) $(INCLUDE) $(DEBUG) $(LFLAGS) -o liblfht.so

liblfht_debug.a: lfht_debug.o
	$(AR) rcu liblfht_debug.a lfht_debug.o

lfht_debug.o: lfht.c
	$(CC) -c lfht.c $(CFLAGS) $(INCLUDE) $(DEBUG) $(LFLAGS) -o lfht_debug.o

clean:
	rm -f *.o *.a *.so
